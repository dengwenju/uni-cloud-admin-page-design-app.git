/*装修组件的类型*/
const designType = {
	img: "img",
	swiper: "swiper",
	line: "line",
	video: "video",
	layer: "layer"
}

export default designType