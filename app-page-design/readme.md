## 【me-page-design】页面装修插件 (APP端)
### 基于uniCloud admin, 可快速自定义装修APP页面, 可配置图片热区,轮播banner,视频,浮层弹窗...

## 安装admin端
admin端集成文档: [https://ext.dcloud.net.cn/plugin?name=me-page-design](https://ext.dcloud.net.cn/plugin?name=me-page-design)

## 安装APP端
1. 更新HBuilderX, 至少3.1.0+版本, 因为使用到uni_modules  
2. 在插件市场打开本插件页面, 在右侧点击`使用 HBuilderX 导入插件`, 选择`uni-app项目`点击确定
3. 在`pages.json`中添加页面路径
```
"pages": [
	// ……其他页面配置
	// APP装修的页面
	{
		"path": "uni_modules/app-page-design/pages/design",
		"style": {
			"navigationStyle": "custom",
			"app-plus" : {
				"titleNView": false
			}
		}
	},
	{
		"path": "uni_modules/app-page-design/pages/design-web",
		"style": {
			"navigationBarTitleText": ""
		}
	}
]
```

## admin端的使用
1. 运行uniCloud admin项目到`Chrome`
2. 初次使用时, 点击`菜单管理`,会自动生成`待添加菜单`, 选中`页面装修`即可添加菜单
3. 点击`页面装修`的菜单,可查看示例,也可按照指引完成APP页面的装修

## APP端的使用
#### 场景一: 打开装修页
```
// 查询别名的方式打开
uni.navigateTo({url: "/uni_modules/app-page-design/pages/design?name=xxx"})
// 查询id的方式打开
// uni.navigateTo({url: "/uni_modules/app-page-design/pages/design?id=xxx"})
```

#### 场景二: tab页是装修页 / 装修组件的单独使用
```
<template>
	<view>
		<app-page-design :res="res"></app-page-design>
	</view>
</template>

<script>
	import {apiDesignDetail} from '@/uni_modules/app-page-design/api/api.js'
	export default {
		data() {
			return {
				res: {}
			}
		},
		onLoad() {
			// 根据name或id查询装修页
			let param = {name: "xxx"} // 或 {id: "xxx"}
			apiDesignDetail(param).then(res=>{
				this.res = res
			})
		}
	}
</script>
```