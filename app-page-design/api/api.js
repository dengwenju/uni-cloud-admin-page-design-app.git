// 根据id或别名name,获取页面装修的详情
export function apiDesignDetail(param) {
	return new Promise((resolve, reject) => {
		// 参数校验
		if(!param.id&&!param.name){
			showToast("缺少参数id或name")
			reject()
			return
		}
		uni.showLoading({title: "加载中..."})
		// 使用ClientDB: https://uniapp.dcloud.io/uniCloud/clientdb
		let where = param.id ? ("_id=='" + param.id+"'") : ("name=='" + param.name+"'")
		const db = uniCloud.database()
		db.collection('me-page-design').where(where).get().then(res=>{
			uni.hideLoading()
			let data = res.result.data[0]
			if(data){
				resolve(data)
			}else{
				showToast("当前装修页面不存在: " + where)
				reject()
			}
		}).catch(e=>{
			uni.hideLoading()
			showToast({code:e.code, msg:e.message})
			reject()
		})
	})
}

/* 提示 */ 
function showToast(msg){
	if(!msg) return
	if(typeof msg == 'object') msg = JSON.stringify(msg)
	uni.showModal({
		title: "温馨提示",
		content: msg,
		showCancel: false,
		success() {
			uni.navigateBack()
		}
	})
}